from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import col, udf, desc , slice
from pyspark.sql.types import *
import ast
import numpy as np
from pyspark.ml.stat import Correlation
from pyspark.ml.feature import VectorAssembler
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import sys as sys

"""
script to obtain dataframe of songs from artist of interest with features of interest
"""

def parse_2d_columns(df, name):
    #Double array array/////
    def lit_eval(line):
        t = ast.literal_eval(line)
        return t
    udf_lines = udf(lambda x: lit_eval(x), ArrayType(ArrayType(DoubleType())))
    return df.withColumn(name, udf_lines(col(name)))

def select_custom_columns(df, columns_list = ['title']):
    columns = [col(x) for x in columns_list]
    return df.select(columns)

sc = SparkContext(appName="getdata")
sc.setLogLevel("ERROR")
spark = SparkSession.builder.getOrCreate()

df = spark.read.csv("/data/doina/OSCD-MillionSongDataset/*.csv", header=True, escape='"', inferSchema=True)
artist_name = ["Snoop Dogg", "Michael Jackson", "Metallica", "Black Sabbath", "Depeche Mode"]
df = df.select(col("artist_name"), col("title"),col("track_id"), col("segments_timbre")).filter(col("artist_name").isin(artist_name))
df = df.dropDuplicates()
df = parse_2d_columns(df, "segments_timbre")
df = df.withColumn("segments_timbre", slice(col("segments_timbre"),100,130))
df.write.option("header", True).json("json_artist_complete_1")
