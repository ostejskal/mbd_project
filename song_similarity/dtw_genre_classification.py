from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import col, udf, desc , slice
import pyspark.sql.functions as F
from pyspark.sql.types import *
import ast
import numpy as np
from pyspark.ml.stat import Correlation
from pyspark.ml.feature import VectorAssembler
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import sys as sys
sc = SparkContext(appName="dtw")
sc.setLogLevel("ERROR")

"""
Genre classification based on the similarity measure,
 basically measuring distance between seelcted song and all others,
 taking the k closest songs and predicting genre as the most frequent genre in the k subset
"""


# Computing distance between df[ts_index] or compared_ts with every other entry in df 
def dtw_distance(df, colname, ts_index = 0, compared_ts = False):
    if not compared_ts:
        compared_ts = df.select(col(colname)).take(ts_index+1)[ts_index][0]
    def one_dtw(ts_b_2d):
        ts_a_2d = compared_ts
        if(len(ts_a_2d) != len(ts_b_2d)):
            return 99999
        suma = 0
        for idx in range(12):
            ts_a = [i[idx] for i in ts_a_2d]
            ts_b = [i[idx] for i in ts_b_2d]
            if(len(ts_a) != len(ts_b)):
                return(99999)
            suma += compute_dtw_distance(ts_a, ts_b)
        return int(suma) if int(suma) != 0 else 99999
    udf_one_dtw = udf(lambda x: one_dtw(x), IntegerType())
    return df.withColumn("dtw_distance_" + colname, udf_one_dtw(col(colname)))

#dtw distance between two timeseries taken from https://github.com/markdregan/K-Nearest-Neighbors-with-Dynamic-Time-Warping
def compute_dtw_distance(ts_a, ts_b, d=lambda x, y: abs(x - y), max_warping_window=10):
    # Create cost matrix via broadcasting with large int
    ts_a, ts_b = np.array(ts_a), np.array(ts_b)
    M, N = len(ts_a), len(ts_b)
    cost = sys.maxsize * np.ones((M, N))
    # Initialize the first row and column
    cost[0, 0] = d(ts_a[0], ts_b[0])
    for i in range(1, M):
        cost[i, 0] = cost[i - 1, 0] + d(ts_a[i], ts_b[0])
    for j in range(1, N):
        cost[0, j] = cost[0, j - 1] + d(ts_a[0], ts_b[j])
    # Populate rest of cost matrix within window
    for i in range(1, M):
        for j in range(max(1, i - max_warping_window),
                        min(N, i + max_warping_window)):
            choices = cost[i - 1, j - 1], cost[i, j - 1], cost[i - 1, j]
            cost[i, j] = min(choices) + d(ts_a[i], ts_b[j])
    # Return DTW distance given window
    return cost[-1, -1]

#ordered dataframe of songs ordering based on dtw distance
def closest_song(name,df_song, df):
    timbre = df_song.filter(col("title") == name).collect()
    timbre = timbre[0][2]
    return dtw_distance(df, "segments_timbre",0, compared_ts = timbre).orderBy("dtw_distance_segments_timbre")

# evaluating model of genre classification based on distance 
def evaluate(df, num, k = 10):
    count = df.count()
    mult = count//num - 2
    collected_df = df.collect()
    hits = 0.
    amp = 0.
    for i in range(num):
        i = i * mult
        try:
            true_genre = collected_df[i][4]
            name = collected_df[i][3]
            closest_df = closest_song(name, df, df).limit(k)
            groups = closest_df.groupby("genre").count()
            mp = float(groups.filter(col("genre") == true_genre).collect()[0][1])/k
            amp += mp
            mfg = groups.sort(col('count').desc()).collect()[0][0]
            print(name,true_genre, mfg, mp)
            if(mfg == true_genre):
                hits +=1
        except Exception as e:
            print(e, i, mult, count)
    return (amp/num, hits/num)


#create a spark session
spark = SparkSession.builder.getOrCreate()

# Function to retrive ordered datframe with column distance, which is difference between values of {colname}
df_genre = spark.read.csv("/user/s2811316/downloaded.csv", sep='\t', inferSchema=True).toDF('track_id', 'genre')
random5000 = "random5000/*.json"
random20k = "random20k/*.json"
df_orig = spark.read.json(random20k)
df = df_orig.join(df_genre, ['track_id'], how='inner')

genre_values = df.select('genre').distinct().collect()
balanced_df = df.limit(1)
threshold = 100
for genre in genre_values:
    genre = genre[0]
    filtered = df.filter(col("genre") == genre).limit(threshold)
    if(filtered.count() == threshold):
        print(genre)
        balanced_df = balanced_df.union(filtered)
    #print(genre, df.filter(col("genre") == genre).count())
evaluate(balanced_df,100, k = 5)

