from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import *
from pyspark.sql.types import *
import ast
import numpy as np

from pyspark.ml.feature import StandardScaler
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.clustering import KMeans
from pyspark.ml.evaluation import ClusteringEvaluator


sc = SparkContext(appName="bench_hashtags")
sc.setLogLevel("ERROR")

"""
by Ondrej Stejskal

kmeans clustering of segments_timbre feature, segments_timbre is a 2d aray,
so I took the mean.
"""

def knn_on_features(df, features_list = [], k = 20):
    #Kmeans create feature vector for Kmeans
    assemble=VectorAssembler(inputCols=features_list, outputCol='features')
    assembled_data=assemble.transform(df)
    #Kmeans standardize
    scale=StandardScaler(inputCol='features',outputCol='standardized')
    data_scale=scale.fit(assembled_data)
    data_scale_output=data_scale.transform(assembled_data)
    #Kmeans fit and make prediction
    KMeans_algo=KMeans(featuresCol='features', k=k)
    KMeans_fit=KMeans_algo.fit(data_scale_output)
    return KMeans_fit.transform(data_scale_output)

knn_df = knn_on_features(df, ["tempo","duration", "timbre_mean"], 20)
knn_df.show(10)

cnt = knn_df.groupby("prediction").count()
# functions used from feature utils

