from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import col, udf, desc , slice
from pyspark.sql.types import *
import ast
import numpy as np
from pyspark.ml.stat import Correlation
from pyspark.ml.feature import VectorAssembler
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import sys as sys

"""
Script to get random data from MSD and selecting interesting columns
"""


def parse_2d_columns(df, name):
    #Double array array/////
    def lit_eval(line):
        t = ast.literal_eval(line)
        return t
    udf_lines = udf(lambda x: lit_eval(x), ArrayType(ArrayType(DoubleType())))
    return df.withColumn(name, udf_lines(col(name)))

def select_custom_columns(df, columns_list = ['title']):
    columns = [col(x) for x in columns_list]
    return df.select(columns)

sc = SparkContext(appName="getdatarand")
sc.setLogLevel("ERROR")
spark = SparkSession.builder.getOrCreate()
url = "/data/doina/OSCD-MillionSongDataset/output_A.csv"
t5000 = "/user/s2893177/train5000.csv"
t2 = "/user/s2893177/train20000.csv"
df = spark.read.csv(url, header=True, escape='"', inferSchema=True)
#artist_name = ["Snoop Dogg"]
df = df.select(col("artist_name"), col("title"), col("segments_timbre"),col("track_id"))
df = df.dropDuplicates()
df = parse_2d_columns(df, "segments_timbre")
df = df.withColumn("segments_timbre", slice(col("segments_timbre"),100,130))
df.write.option("header", True).json("randomoutputafinal")
