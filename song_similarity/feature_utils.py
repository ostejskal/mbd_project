from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import *
from pyspark.sql.types import *
import ast
import numpy as np

from pyspark.ml.stat import Correlation
from pyspark.ml.feature import VectorAssembler


"""
Useful Functions to work with the MSD dataset. 
"""

# Function to retrive ordered datframe with column distance, which is difference between values of {colname}
def get_distance(df,colname, num = 0):
    if(num == 0):
        num = df.select(col(colname)).take(1)[0][0]
    df = df.withColumn("distance", abs(col(colname) - num))
    df = df.orderBy(col('distance'))
    return df


# returns df only with columns specified as list in second argument
def select_custom_columns(df, columns_list = ['title']):
    columns = [col(x) for x in columns_list]
    return df.select(columns)
    

# returns tuple (columns-names, pearson correlation matrix)
def correlation_matrix(df, columns = []):
    vector_col = "features"
    assembler = VectorAssembler(inputCols=columns, outputCol=vector_col)
    df_vector = assembler.transform(df).select(vector_col)
    matrix = Correlation.corr(df_vector, vector_col)
    return (columns, matrix.collect()[0]["pearson({})".format(vector_col)].values)

#Double array array/////
def lit_eval(line):
     t = ast.literal_eval(line)
     return t

def parse_2d_columns(df, name):
    #Double array array/////
    def lit_eval(line):
        t = ast.literal_eval(line)
        return t
    udf_lines = udf(lambda x: lit_eval(x), ArrayType(ArrayType(DoubleType())))
    return df.withColumn(name, udf_lines(col(name)))

def add_artist_terms(df):
    return df.withColumn("artist_terms", split(regexp_replace(regexp_replace(col("artist_terms"), r'"', ""),r'\[', ""),",").cast(ArrayType(StringType())))

def add_similar_artists(df):
    return df.withColumn("similar_artists", split(regexp_replace(regexp_replace(col("similar_artists"), r'"', ""),r'\[', ""),",").cast(ArrayType(StringType())))

#Create mean from 2d arr
def two_d_mean(multi_arr):
    temp_sum = 0
    temp_count = 0
    for arr in multi_arr:
        for el in arr:
            if el is not None:
                temp_sum += el
                temp_count += 1
    ret = temp_sum/temp_count            
    if type(ret) == int or type(ret) == float:
        return ret
    else:
        return 0


def add_timbre_mean(df):
    udf_two_d_mean = udf(lambda a: two_d_mean(a), DoubleType())
    return df.withColumn("timbre_mean", udf_two_d_mean(col('segments_timbre')))
    

#Load data
spark = SparkSession.builder.getOrCreate()
df = spark.read.csv("train1000.csv", header=True, escape='"', inferSchema=True)


#------------------------------ TESTS -----------------------------#

#Select columns test
selected_columns = ["artist_name", "title", "tempo", "duration", "segments_timbre"]
df = select_custom_columns(df,selected_columns)
df.show(10) 

#get_distance test
get_distance(df,"tempo").show(10)

#correlation_matrix test
cm = correlation_matrix(df, ["tempo", "duration"])

#timbre test
df = parse_2d_columns(df, "segments_timbre")
df = add_timbre_mean(df)

