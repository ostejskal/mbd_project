#nb. first move files from hdfs ('/user/s2882930/lyrics_genre1') to your machine
import ast
import os
from os import walk, path, rename
import shutil
from glob import glob
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import matplotlib.pyplot as plt
import nltk
from nltk.corpus import stopwords
import pathlib
import pandas as pd

#we now we reorganize the results locally

#given a directory with >2 subdirectories, each file is assigned its subdirectory name (possibly mantaining part of the name of the original file)
parent = ("results/")
for dirpath, _, files in walk(parent):
    for f in files:
        #newf = f.replace("genre=",os.path.basename(dirpath)+"_")
        #rename(path.join(dirpath, f), path.join(dirpath,newf))
        rename(path.join(dirpath, f), path.join(dirpath, path.split(dirpath)[-1]) + ".csv") #add "+ f" in path.split(dirpath)[-1] add the original file name to the folder name
        

#extract files from subdirectories, moves them to the parent directory and deletes the subdirectories
folder = r"results/"
subfolders = [f.path for f in os.scandir(folder) if f.is_dir()]
for sub in subfolders:
    for f in os.listdir(sub):
        src = os.path.join(sub, f)
        dst = os.path.join(folder, f)
        shutil.move(src, dst)
        shutil.rmtree(sub)


#create word cloud for each file in path
stopwords = list(set(stopwords.words('english')))
PATH = "results/"
extension = "*.csv"
csv_files = [file
                 for path, subdir, files in os.walk(PATH)
                 for file in glob(os.path.join(path, extension))]
os.makedirs('results/plots/')
for i in csv_files:
    df = pd.read_csv(i, header = None)
    df = df[~df.iloc[:,0].isin(stopwords)]
    dic = dict(zip(df.iloc[:,0], df.iloc[:,1]))    
    wordcloud = WordCloud(max_font_size=50, max_words=100, background_color="white", stopwords=stopwords, collocations=False).generate_from_frequencies(dic)
    path = pathlib.PurePath(i)
    title = path.name
    #plt.figure()
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    #plt.show()    
    plt.savefig('results/plots/' + title.replace('.csv', '.png').replace('genre=', ''))
