#### LYRICS

from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import *
from pyspark.sql.types import *
import ast

#lyrics table
# load mxm_dataset.db on hdfs
import sqlite3
import csv
conn = sqlite3.connect('mxm_dataset.db')
cursor = conn.cursor()
cursor.execute("SELECT * FROM words INNER JOIN lyrics ON words.word = lyrics.word")
with open('lyrics.csv','w') as out_csv_file:
  csv_out = csv.writer(out_csv_file)
  # write header                        
  csv_out.writerow([d[0] for d in cursor.description])
  # write data                          
  for result in cursor:
    csv_out.writerow(result)
conn.close()

#import lyrics table
df = spark.read.csv("lyrics.csv", header=True, escape='"', inferSchema=True)
#import genre table
df_genre = spark.read.csv("/user/s2811316/downloaded.csv", sep='\t', inferSchema=True).toDF('track_id', 'genre')
#join lyrics and genre tables
df1 = df.join(df_genre, ['track_id'], how='inner')
#find word occurence by genre
df2 = df1.groupBy('genre', 'word0').agg(sum('count').alias("count")).withColumnRenamed("word0", "word")
#copy column 
df2 = df2.withColumn("genre0", df2["genre"])
#write results on hdfs
df2.repartition('genre0').write.partitionBy('genre0').csv("/user/s2882930/results")


