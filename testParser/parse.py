from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import *
from pyspark.sql.types import *
import ast

sc = SparkContext(appName="bench_hashtags")
sc.setLogLevel("ERROR")

#create a spark session
spark = SparkSession.builder.getOrCreate()
df = spark.read.csv("train1000.csv", header=True, escape='"', inferSchema=True)

#String array//////
df = df.withColumn("artist_terms", split(regexp_replace(regexp_replace(col("artist_terms"), r'"', ""),r'\[', ""),",").cast(ArrayType(StringType())))
df = df.withColumn("similar_artists", split(regexp_replace(regexp_replace(col("similar_artists"), r'"', ""),r'\[', ""),",").cast(ArrayType(StringType())))

#Double array///////
arrayDoubleList = ['artist_terms_freq','artist_terms_weight','bars_confidence','bars_start', 'beats_confidence', 'beats_start', 'release_7digitalid', 'sections_confidence', 'sections_start', 'segments_confidence', 'segments_loudness_max', 'segments_loudness_max_time', 'segments_loudness_start', 'segments_start']
for c in arrayDoubleList:
     df =df.withColumn(c, split(regexp_replace(regexp_replace(col(c), r'"', ""),r'\[', ""),",").cast(ArrayType(DoubleType())))

#Double array array/////
def lit_eval(line):
     t = ast.literal_eval(line)
     return t


udf_lines = udf(lambda x: lit_eval(x), ArrayType(ArrayType(DoubleType())))
df = df.withColumn("segments_pitches", udf_lines(col("segments_pitches")))
df = df.withColumn("segments_timbre", udf_lines(col("segments_timbre")))


print df.show()
