from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import *
from pyspark.sql.types import *
import ast
import numpy as np
import argparse


from pyspark.ml.linalg import VectorUDT, Vectors
from pyspark.ml.stat import Correlation
from pyspark.ml.feature import StandardScaler, StringIndexer, VectorAssembler
from pyspark.ml.clustering import KMeans
from pyspark.ml.classification import DecisionTreeClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
import math

LOAD_PATH = "train1000"
CHUNKS = 1
SAVE_PATH = "p/kmeans/"+LOAD_PATH+"/ch="+str(CHUNKS)

parser = argparse.ArgumentParser()
parser.add_argument("--input", help="input path")
parser.add_argument("--chunks", help="number of chunks")
parser.add_argument("--trainsize", help="percentage of data used for training")

args = parser.parse_args()
if args.input:
    LOAD_PATH = args.input
if args.chunks:
    CHUNKS = int(args.chunks)
if args.trainsize:
    TRAIN_SIZE = float(args.trainsize)

SAVE_PATH = "p/dectree/"+LOAD_PATH+"/ch="+str(CHUNKS)


#This method vectorizes and normalizes the feature column and indexes the label column
def vectorize_index_and_scale(df, inputCols):
    stringIndexer = StringIndexer(inputCol="genre", outputCol="genre_indexed")
    fit_indexed = stringIndexer.fit(df)
    indexed = fit_indexed.transform(df)
    assembler = VectorAssembler(inputCols=inputCols, outputCol="raw_features")
    assembled = assembler.transform(indexed)
    scaler=StandardScaler(inputCol='raw_features',outputCol='features')
    data_scaler = scaler.fit(assembled)
    return data_scaler.transform(assembled)

#Interperate string as literal (e.g. string -> array)
def lit_eval(line):
     t = ast.literal_eval(line)
     return t


#Takes list and number of CHUNKS and chunks accordingly
def chunk_avg(l, n):
    #n = number of CHUNKS
    #l = 2d-array
    c_size = int(math.floor(len(l)/n)) #floor because CHUNKS need the same size
    #To maintain similar matrix sizes take list up until n
    return [np.mean(l[i:i+c_size], axis=0).tolist() for i in xrange(0, len(l), c_size)][:n] 


sc = SparkContext(appName="timbre_chunked_average")
sc.setLogLevel("ERROR")
spark = SparkSession.builder.getOrCreate()

#Read csv to dataframe. Escape indicator is used for properly parsing the array columns from string to array
df = spark.read.csv(LOAD_PATH+".csv", header=True, escape='"', inferSchema=True)

#Join the genre dataset
df_genre = spark.read.csv("/user/s2811316/downloaded.csv", sep='\t', inferSchema=True).toDF('track_id', 'genre')
df = df.join(df_genre, ["track_id"])

#Convert the nested array to proper type 
udf_lines = udf(lambda x: lit_eval(x), ArrayType(ArrayType(DoubleType())))
df = df.withColumn("segments_timbre", udf_lines(col("segments_timbre")))

#Chunk the timbre_segments column into CHUNKS chunks
ch_udf = udf(chunk_avg, ArrayType(ArrayType(DoubleType())))
df = df.withColumn("chunked_avg", ch_udf(col("segments_timbre"),lit(CHUNKS)).alias("chunked_avg"))

#Vectorize each chunked column 
to_vec = udf(lambda x: Vectors.dense(x), VectorUDT())
cols = [to_vec(col("chunked_avg")[i]).alias("ch{}".format(i)) for i in xrange(CHUNKS)]

#Select all columns again to apply vectorization of chunks. This method is  chosen because withColumn only takes one column
cols.extend(df.columns)
df = df.select(*cols)

#Create list of feature columns
inputCols=["ch{}".format(i) for i in xrange(CHUNKS)]

dt = DecisionTreeClassifier(labelCol="genre_indexed")
df_processed = vectorize_index_and_scale(df, inputCols)
#Split into train-test data
df_train, df_test = df_processed.randomSplit([TRAIN_SIZE, 1-TRAIN_SIZE])

dt_model = dt.fit(df_train)

dt_result = dt_model.transform(df_test)

#Evaluate results
evaluator = MulticlassClassificationEvaluator(predictionCol="prediction", labelCol="genre_indexed")
res = evaluator.evaluate(dt_result)
print res 

#Append to output file
spark.createDataFrame([('dec_tree', CHUNKS, res)]).write.mode("append").csv("p/output")


