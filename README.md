# MBD_project

## Research questions:

(Emiel, Ondrej, Ruben)
* Define a similarity measures among songs based on the audiofeatures present in the dataset. Retrieve the most similar songs and visualize how they position themselves among the principal components of the audiofeatures 
* Determining the genre of a song based on audio features apply cluster analysis to the main factors of the audiofeatures and understand what role they play in influencing the clusters → could be an indication of the audio-features that play a role in defining music genres.
* Analyzing trends in genre and find any patterns to predict new trends: is there a seasonality in the trends or are trends reappearing How do genres change over the years

(Ellia, Stefano)
* Finding artists which are referenced a lot by other artists but are not popular?

* NLP tasks: supervised learning → genre classification based on lyrics (Word2Vec?)
Wordcloud


## Progress
Please write down on what are you working:

### Ondrej:

* feauture_extraction/k-mean_clustering clustering songs based on the segments_timbre and finding most similar songs

### Ruben:

### Emiel:

### Ellia:

### Stefano:

