from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import *
from pyspark.sql.types import *
import ast

sc = SparkContext(appName="bench_hashtags")
sc.setLogLevel("ERROR")

#create a spark session
spark = SparkSession.builder.getOrCreate()
#df = spark.read.csv("test100k.csv", header=True, escape='"', inferSchema=True)
df = spark.read.csv("/data/doina/OSCD-MillionSongDataset/*.csv", header=True, escape='"', inferSchema=True)

df_track_years = spark.read.csv("tracks_per_years.csv", sep='\t', inferSchema = True).toDF('year', 'track_id', 'artist_name', 'title').select('year', 'track_id')

df_genre = spark.read.csv("downloaded.csv", sep='\t', inferSchema=True).toDF('track_id', 'genre')

#print(df_track_years.head(10))

#String array//////
df = df.withColumn("artist_terms", split(regexp_replace(regexp_replace(col("artist_terms"), r'"', ""),r'\[', ""),",").cast(ArrayType(StringType())))
df = df.withColumn("similar_artists", split(regexp_replace(regexp_replace(col("similar_artists"), r'"', ""),r'\[', ""),",").cast(ArrayType(StringType())))

#Double array///////
arrayDoubleList = ['artist_terms_freq','artist_terms_weight','bars_confidence','bars_start', 'beats_confidence', 'beats_start', 'release_7digitalid', 'sections_confidence', 'sections_start', 'segments_confidence', 'segments_loudness_max', 'segments_loudness_max_time', 'segments_loudness_start', 'segments_start']
for c in arrayDoubleList:
     df =df.withColumn(c, split(regexp_replace(regexp_replace(col(c), r'"', ""),r'\[', ""),",").cast(ArrayType(DoubleType())))

#Double array array/////
def lit_eval(line):
     t = ast.literal_eval(line)
     return t


udf_lines = udf(lambda x: lit_eval(x), ArrayType(ArrayType(DoubleType())))
df = df.withColumn("segments_pitches", udf_lines(col("segments_pitches")))
df = df.withColumn("segments_timbre", udf_lines(col("segments_timbre")))

#drop "nan" on "song_hotttnesss"
df = df.where("song_hotttnesss != 'nan'")

#Integrate with genre label
df_res = df.join(df_genre, ['track_id'], how='inner').select('artist_mbid', 'artist_name', 'year', 'song_hotttnesss', 'track_id')

#Keep only tracks with year
df_years = df_res.join(df_track_years, ['year', 'track_id'], how='inner')

#Group by
df_res = df_years.groupBy('artist_mbid', 'artist_name', 'year', 'genre').agg(avg("song_hotttnesss").alias('hotness_avg'), count("song_hotttnesss").alias('song_count')).orderBy('artist_name', 'year')
#df_res = df_years.select('artist_mbid', 'artist_name', 'year', 'song_hotttnesss').orderBy('year', 'artist_name')

#df_res.show(20)
df_res.coalesce(1).write.option("header", True).csv("output/hotness_timeline")