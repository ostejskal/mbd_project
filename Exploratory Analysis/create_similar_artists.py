from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql.functions import *
from pyspark.sql.types import *
import ast
from pyspark.ml.fpm import FPGrowth


sc = SparkContext(appName="bench_hashtags")
sc.setLogLevel("ERROR")

#create a spark session
spark = SparkSession.builder.getOrCreate()
'''
df_ids = spark.createDataFrame([
    ("AR002UA1187B9A637D", ["AR002UA1187B9A637D", "AR003FB1187B994355", "AR009SZ1187B9A73F4"]),
    ("AR002UA1187B9A637D", ["AR002UA1187B9A637D", "AR003FB1187B994355", "AR006821187FB5192B", "AR009SZ1187B9A73F4"]),
    ("AR002UA1187B9A637D", ["AR002UA1187B9A637D", "AR003FB1187B994355"])
], ["artist_echoid", "similar_artists"])
'''

'''
df_ids = spark.createDataFrame([
    (0, [1, 2, 5]),
    (1, [1, 2, 3, 5]),
    (2, [1, 2])
], ["id", "similar_artists"])
'''


#df = spark.read.csv("/data/doina/OSCD-MillionSongDataset/*.csv", header=True, escape='"', inferSchema=True)
df = spark.read.csv("test.csv", header=True, escape='"', inferSchema=True)

#df_track_years = spark.read.csv("tracks_per_years.csv", sep='\t', inferSchema = True).toDF('year', 'track_id', 'artist_name', 'title').select('year', 'track_id')
df_artist_id = spark.read.csv("artist_id.csv", sep='\t', inferSchema = True).toDF('artist_echoid', 'artist_mbid', 'track_id', 'artist_name').select('artist_echoid', 'artist_mbid', 'track_id')

#df_artist_id.show(20)
#exit(0)
#print(df_track_years.head(10))

#String array//////
df = df.withColumn("artist_terms", split(regexp_replace(regexp_replace(col("artist_terms"), r'"', ""),r'\[', ""),",").cast(ArrayType(StringType())))
df = df.withColumn("similar_artists", split(regexp_replace(regexp_replace(col("similar_artists"), r'"', ""),r'\[', ""),",").cast(ArrayType(StringType())))

#Double array///////
arrayDoubleList = ['artist_terms_freq','artist_terms_weight','bars_confidence','bars_start', 'beats_confidence', 'beats_start', 'release_7digitalid', 'sections_confidence', 'sections_start', 'segments_confidence', 'segments_loudness_max', 'segments_loudness_max_time', 'segments_loudness_start', 'segments_start']
for c in arrayDoubleList:
     df =df.withColumn(c, split(regexp_replace(regexp_replace(col(c), r'"', ""),r'\[', ""),",").cast(ArrayType(DoubleType())))

#Double array array/////
def lit_eval(line):
     t = ast.literal_eval(line)
     return t


udf_lines = udf(lambda x: lit_eval(x), ArrayType(ArrayType(DoubleType())))
df = df.withColumn("segments_pitches", udf_lines(col("segments_pitches")))
df = df.withColumn("segments_timbre", udf_lines(col("segments_timbre")))

df = df.select('artist_mbid', 'similar_artists', 'artist_name')
#Join df on artist_mbid
df_ids = df.join(df_artist_id, 'artist_mbid', how='inner').select('artist_echoid', "similar_artists")

#df_ids = df_ids.take(50)
df_ids.printSchema()

#Check missing
#df_ids.select([count(when(col('similar_artists').isNull(),True))]).show()

df_ids.show(20)

#exit(0)

#Apply fpGrowth
fpGrowth = FPGrowth(itemsCol='similar_artists', minSupport=0.001, minConfidence=0)
model = fpGrowth.fit(df_ids)

# Display frequent itemsets.
df_item = model.freqItemsets#.show(truncate = False)
df_item.show()

#Filter for length two
df_filtered = df_item.filter(size(df_item.items) == 2)
print("Filter Done")
df_filtered.show(truncated = False)

exit(0)

# Display generated association rules.
model.associationRules.show()



# transform examines the input items against all the association rules and summarize the
# consequents as prediction
model.transform(df_ids).show()
'''
#Keep only tracks with year
#.distinct() to keep only one entry for each track_id (They would have same and repeated duration). This can be done before join for better perf
df_years = df.join(df_track_years, ['year', 'track_id'], how='inner').select('track_id', 'duration', 'year').distinct()

#df_years.where("year == 1934").select("year","duration").show()
#If there's only one element, NaN is return from stddev. Fill it with 0

#Group by
df_res = df_years.groupBy('year').agg(mean("duration").alias('duration_avg'), stddev(col("duration")).alias("stddev_avg")).na.fill({'stddev_avg':0}).orderBy('year')
#df_res = df_years.select('artist_mbid', 'artist_name', 'year', 'song_hotttnesss').orderBy('year', 'artist_name')

df_res.show(20)
df_res.coalesce(1).write.option("header", True).csv("output/duration_timeline")
'''
